# SaltStack formula template

Just an example for a Salt formula around node. Ships with Kitchen Test and Jenkinsfile pipeline.

## Available state

`node`: Install NodeJS 8 from Debian Stretch repository

## Variables

None

## Example pillar

None

## Tests

Install docker, then

```bash
sudo apt-get install -y python-pip python-virtualenv rubygems docker-ce build-essential libffi-dev autoconf
gem install bundler
bundle install --without test development
virtualenv .venv
source .venv/bin/activate
pip install -r ./test/requirements.txt
deactivate
```

Run all tests.

```bash
source .venv/bin/activate
bundle exec kitchen test
```

See `kitchen --help` to run specific step on specific platform, etc

