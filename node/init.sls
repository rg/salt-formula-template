repo:
  pkgrepo.managed:
    - humanname: nodesource
    - name: deb http://deb.nodesource.com/node_8.x/ stretch main
    - file: /etc/apt/sources.list.d/nodesource.list

nodejs:
  pkg.installed:
    - refresh: True
    - requires:
      - pkgrepo: repo
