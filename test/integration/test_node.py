# Here are some commented examples
# If you need requests, yaml or any other deps, add it in test/requirements.txt
#
# import pytest
# import requests
# import yaml
# import os
#
#
# @pytest.mark.parametrize(
#     "name",
#     ["python-pip", "python3-setuptools", "python3-dev", "python3-pip", "virtualenv"],
# )
# def test_packages(host, name):
#     p = host.package(name)
#     assert p.is_installed
#
#
# def test_user(host):
#     u = host.user("cozy")
#     assert u.name == "cozy"
#     assert u.home == "/var/lib/cozy"
#     assert u.shell == "/bin/bash"
#
#
# @pytest.mark.parametrize(
#     "path",
#     [
#         "/backup/",
#         "/etc/hosts",
#         "/root/.ssh",
#         "/root/.ssh/id_rsa.pub",
#         "/root/.ssh/id_rsa",
#     ],
# )
# def test_files(host, path):
#     f = host.file(path)
#     assert f.exists
#     assert f.user == "root"
#     assert f.group == "root"
#
#
# def test_depending_on_pillar_value(host):
#     pillar = yaml.load(host.file("/tmp/kitchen/srv/pillar/pillar.sls").content_string)
#     if "enabled" in pillar["app"] and bool(pillar["app"]["enabled"]):
#         assert host.file("/opt/app").exists
#
#     else:
#         assert not host.file("/opt/app").exists
#
#
# def test_service(host):
#     s = host.service("ssh")
#     assert s.is_running
#     assert s.is_enabled
#
#
# def test_app_version(host):
#     ip = host.interface("eth0").addresses[0]
#     r = requests.get("http://" + ip + ":8080/version")
#     assert r.status_code == 200
#     assert r.json()["build_mode"] == "production"
#
#
# def test_command_rc_on_host(host):
#     ip = host.interface("eth0").addresses[0]
#     # This command is executed on the testing host
#     # not inside the tested container.
#     r = os.system("ping -c 1 " + ip)
#     assert r == 0
#
#
# def test_command_rc_in_container(host):
#     # This command is executed inside the tested container.
#     cmd = host.run("ls /etc/pass* > /dev/null")
#     assert cmd.rc == 0
#
#
# def test_command_output(host):
#     help = host.check_output("/usr/local/bin/script --help")
#     assert "caddy" in help
#     assert "traefik" in help


def test_not_implemented(host):
    assert False

def test_package(host):
    assert host.package("nodejs").is_installed


def test_version(host):
    assert "v8" in host.check_output("nodejs --version")
