/* Jenkins pipeline, declarative syntax
 *
 * Plugins requirements:
 *  - Gitlab
 *  - Mattermost Notification
 *  - AnsiColor
 *
 * Agent requirements:
 *  - Docker
 *  - Python (pip, virtualenv)
 *  - Ruby (rbenv, bundler, gem)
 *
 * Documentation:
 *  - https://jenkins.io/doc/book/pipeline/#declarative-versus-scripted-pipeline-syntax
 *  - https://jenkins.io/doc/book/pipeline/syntax/
 *
 */
pipeline {

  agent {
    label 'kitchen'
  }

  options {
    disableConcurrentBuilds()
    // timestamps()
  }

  stages {

    stage ('Setup environment') {
      steps {
        checkout scm
        sh '''
        echo "SETUP"
        virtualenv .venv
        . .venv/bin/activate
        pip install -r ./test/requirements.txt
        sudo /usr/local/rbenv/exec bundle install
        echo "CLEANING LEFTOVERS FROM PREVIOUS RUN IF ANY"
        /usr/local/rbenv/exec bundle exec kitchen destroy
        rm -f mattermostSend/*test_report*
        echo "DISPLAY VERSIONS"
        docker -v
        python -V
        /usr/local/rbenv/exec ruby --version
        /usr/local/rbenv/exec gem --version
        /usr/local/rbenv/exec bundle version
        /usr/local/rbenv/exec bundle exec kitchen version
        '''
      }
    }

    stage ('Tests') {
      steps {

        // Scripted pipeline block, to be able to generate stage using a loop
        // https://jenkins.io/doc/book/pipeline/syntax/#script
        script {
          // Here's the only thing you might want to adapt, the targets list
          targets = [
            'default-stretch-nitrogen-py2',
            'default-buster-nitrogen-py2',
            // 'default-xenial-nitrogen-py2',
            // 'default-bionic-nitrogen-py2',
            'default-stretch-fluorine-py2',
            'default-buster-fluorine-py2',
            'default-stretch-fluorine-py3',
            'default-buster-fluorine-py3',
            // 'default-xenial-fluorine-py2',
            // 'default-bionic-fluorine-py2',
            // 'default-xenial-fluorine-py3',
            // 'default-bionic-fluorine-py3',
            ]
          tests_ok = 0
          gitlabBuilds(builds: targets) {
            for (int i = 0 ; i < targets.size() ; i++) {
              updateGitlabCommitStatus name: "${targets[i]}", state: 'running'
              stage ("Kitchen converge ${targets[i]}") {
                ansiColor('css') {
                  converge_ok = sh (
                    returnStatus: true,
                    script: """
                      set +xe
                      . .venv/bin/activate
                      /usr/local/rbenv/exec bundle exec kitchen converge ${targets[i]}
                    """
                  )
                }
                tests_ok += converge_ok
                if (converge_ok != 0) {
                  unstable ('Converge failed')
                }
              }
              stage ("Kitchen verify ${targets[i]}") {
                if (converge_ok != 0) {
                  verify_ok = 0
                  skip = sh (
                    returnStatus: true,
                    script: """
                      false
                    """
                  )
                  unstable ('Verify skipped')
                } else {
                  ansiColor('css') {
                    verify_ok = sh (
                      returnStatus: true,
                      script: """
                        set +xe
                        . .venv/bin/activate
                        /usr/local/rbenv/exec bundle exec kitchen verify ${targets[i]}
                      """
                    )
                  }
                  tests_ok += verify_ok
                  if (verify_ok != 0) {
                    unstable ('Verify failed')
                  }
                }
              }
              if (converge_ok != 0 || verify_ok != 0) {
                updateGitlabCommitStatus name: "${targets[i]}", state: 'failed'
              } else {
                updateGitlabCommitStatus name: "${targets[i]}", state: 'success'
              }
            }
            if (tests_ok == 0) {
              currentBuild.result = "SUCCESS"
            } else {
              currentBuild.result = "FAILURE"
            }
          }
        }
      }
    }
  }

  post {
    failure {
      mattermostSend(color: "danger", message: "<${env.BUILD_URL}|${env.JOB_NAME} build ${env.BUILD_NUMBER} triggered by ${env.gitlabUserName}> failed")
    }
    aborted {
      script {
        for (int i = 0 ; i < targets.size() ; i++) {
          updateGitlabCommitStatus name: "${targets[i]}", state: 'canceled'
        }
      }
    }
    always {
      junit 'test/*.xml'
    }
    cleanup {
      sh (
        script: """
          set +xe
          . .venv/bin/activate
          /usr/local/rbenv/exec bundle exec kitchen destroy
        """
      )
    }
  }
}
